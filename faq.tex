\documentclass[11pt]{article}

% Layout
\usepackage{setspace}
\usepackage[margin=3cm]{geometry}

% Imports
\usepackage{isotope}

% ERT-specific notation
\newcommand{\stlc}{\(\lambda_{\ms{stlc}}\)}
\newcommand{\ert}{{\(\lambda_{\ms{ert}}\)}}

% Bibliography resources
\addbibresource{references.bib}

% Q&A Format
\newcounter{questioncounter}
\newcommand{\question}[1]{
    \stepcounter{questioncounter}
    \item[\textbf{Q\thequestioncounter:}]\noindent\textbf{#1}
}
\newcommand{\answer}[1]{\item[\textbf{A:}] #1}
\newcommand{\qa}[2]{{
    \item[] \begin{itemize}[leftmargin=*]
        \question{#1}
        \answer{#2}
    \end{itemize}
}}

\title{\tty{isotope} FAQ}
\author{Jad Elkhaleq Ghalayini}

\begin{document}

\maketitle

\begin{itemize}[leftmargin=*]

\qa{
    Why should one ``explore the usage of refinement typing and categorical semantics in the context of a low-level intermediate representation for systems programming"?
}{
    One of the reasons refinement typing is effective for large-scale verification efforts is that it is compositional: unlike in dependent type theory, where the computational behavior of a function matters for verification (hence, changing a function's body can break proofs!), most refinement type systems essentially encapsulate functions such that callers can only use the information in their specifications for verification. Hence, the verification problem becomes much more tractable since all we need to do is check that a function satisfies its specification assuming all functions it calls satisfies its specification. Furthermore, it makes it possible to do software engineering since developers can change the implementation of functions and even whole modules so long as the new implementations still adhere to the original specification. Unfortunately, while refinement typing is compositional, the specifications for low-level languages are not, especially in a concurrent setting.
    \begin{itemize}
        \item \emph{Horizontal composition} is absent from the sequential setting but still implicitly there (see: instruction reordering, dataflow dependencies, frame rule, Rust); in the parallel setting, it is ill-specified.
        \item \emph{Sequential composition} works well in the sequential setting but is highly nontrivial in the concurrent setting: see \cite{leaky-semicolon}.
        \item \emph{Separation logic} imposes compositionality onto non-compositional semantics. Hence, refinement-type systems can be compositional even though the underlying model is not.
        \item Programming is already done and conceived of in a compositional way (e.g., subroutines, modules), and having an IR and semantics which reflects that makes it easier to convert informal to formal reasoning.
        \item Having the semantics closer to the refinement type system makes it easier to incorporate refinement information into optimizations, making things like the \tty{abstract} operator possible.
    \end{itemize}
    Essentially: refinement typing for systems programming works because it allows us to reason about systems programs compositionally. However, the semantics of the languages are usually not compositional, so it is challenging to design a refinement typing system, though approaches, such as separation logic, exist to tackle this. Categorical semantics help us build languages that have compositional semantics since categories are all about composition; also, the techniques in \cite{ftrs} allow us to closely relate our semantics and our refinement type system by defining a functor. Also, compositional things are simpler to reason about, which is always nice!

    \TODO{Textify the above, revise}
}

\qa{
    Is a common abstraction above ``a wide range of underlying platforms and memory models" viable?  
    Or, if it is, does existing LLVM or C/C++ not provide that?
}{
    \begin{itemize}
        \item LLVM is a common abstraction over various underlying platforms and memory models, so making such an abstraction is viable. However, it still needs precise semantics, and many of its optimizations break in a concurrent setting. Furthermore, it suffers from the disadvantages of SSA, including not being particularly normalizing, not having a native notion of parallel composition, and needing to restore SSA form every pass. It's also not compositional. That said, we're not competing with LLVM but instead trying to build on it: we still plan to use LLVM as a backend and potentially as a frontend for our IR. The IR's purpose is to make it easier to define optimizations and reason about their correctness on all these various platforms. That is very difficult to do in LLVM due to the lack of compositionality (so you need to accomplish some global reasoning) and precise semantics. Making a common abstraction which is also elegant and compositional, \emph{may} be viable; figuring out if it \emph{is} is the point of the \tty{isotope} project!
        \item C++ and C are not, in my opinion, such abstractions. While you can use them as one, the semantics derive from the set of optimizations we want to be valid, and this set of optimizations corresponds to what should be valid on essentially a single-core PDP-11. Given the historical context, this makes perfect sense. However, we want to explore new ideas by considering new semantics defined from the ground up around a modern, concurrent framework; hence, it is helpful to have a language and execution model built up around our semantics rather than vice versa.
    \end{itemize}
    \TODO{Textify the above, revise}
}

\qa{
    Why do we need a new intermediate representation? 
}{
    \begin{itemize}
        \item Why a new thing at all:
        \begin{itemize}
            \item Existing IRs such as LLVM mostly have no official semantics beyond an English-language specification, but instead, competing ad-hoc semantics exist.
            \item We might not make better new semantics than existing work, and backward compatibility/technical debt is burdensome.
            \item But a new IR means something other than starting from scratch since we plan to support an LLVM backend and integration with LLVM overall.
            \item Similarly, switching to a new IR format doesn't mean starting from scratch, as interconverting between SSA and RVSDG is fast.
        \end{itemize}
        \item Why an RVSDG:
        \begin{itemize}
            \item Restoring SSA form is hard; the RVSDG avoids this.
            \item Computing dataflow dependencies is hard; the RVSDG makes this simpler.
            \item The RVSDG is naturally compositional, making it easier to define our semantics. The RVSDG also supports a natural notion of parallel composition.
            \item The RVSDG is highly normalizing since it abstracts over the ordering of independent instructions.
        \end{itemize}
    \end{itemize}
    \TODO{textify the above, revise}
}

\qa{
    Why specifically for \emph{systems} programs, and how will the RVSDG support them specifically?
}{
    \begin{itemize}
        \item Building an IR for a high-level language means being tied to that particular language's runtime or its type system and execution model. Not all languages have GC; not all languages are functional, etc.
        \item Every compiled language eventually becomes a systems program; even JITted languages do so to a degree.
        \item Therefore, we achieve maximal generality by making our IR a systems programming language.
        \item While we can use our system of refinement types to prove things about systems programs, higher-level languages stand to gain the most significant advantages since their compilers can automatically generate refinements. For example, a high-level numerical language can indicate that a calculation has no side effects or implements a given high-level specification. We can verify these facts and use them to guide optimizations, increasing safety and efficiency; furthermore, these advantages do not require adding any refinements to the high-level source language.
        \item Beyond generality, using a systems programming language also means that we can verify and optimize the runtime functions generated code for the high-level program itself calls using the same system.
        \item We intend refinement-guided optimizations to permit performing high-level, and low-level optimizations using the same framework and make it easier for compiler writers to introduce semantically sound optimization passes into their high-level languages. If our IR were not a systems language, integrating low-level optimizations into the pipeline would be unfeasible.
    \end{itemize}
    \TODO{textify the above, revise}
}

\qa{
    Why does it need an \emph{axiomatic}, \emph{categorical} semantics in particular?
}{
    \begin{itemize}
        \item Why categorical:
        \begin{itemize}
            \item We want categorical semantics because we want our semantics to be compositional and therefore form a category. A monoidal category is a natural fit as we want to support parallel and sequential composition.
            \item Categorical semantics also lets us approach the design of a refinement-type system in a principled way by using the techniques in \cite{ftrs}.
            \item Categorical semantics also naturally fit the structure of the RVSDG
        \end{itemize}
        \item Why axiomatic:
        \begin{itemize}
            \item We want our categorical semantics to be axiomatic to make it easier to extend our semantics with new axioms and consider diverse models. For example, introducing the abstract operator or concurrent primitives would require changing the underlying model but may otherwise preserve the validity of many axiomatically-derived optimizations.
            \item It is simpler to list out the required properties of our model as axioms, which we may then verify than to attempt to derive useful properties from a predetermined model. That is, the axioms can help us select good models.
        \end{itemize}
    \end{itemize}
    \TODO{textify the above, revise}
}

\qa{
    How will these refinement-guided optimisations over \tty{isotope} relate to existing compiler optimisations 
    - in what way will they be better?
}{
    \begin{itemize}
        \item We intend the proposed nucleus refinement-guided optimizations to be analogous to high-level rather than low-level optimizations since the high-level specifications of isotope code guide them rather than their implementation. The key feature is that, in tandem with the abstract operator, they give a framework for defining sound user-defined optimizations.
        \item The eventual goal is to allow high-level and low-level optimization within a single framework, with traditional low-level optimizations performed in tandem with refinement-guided high-level optimizations using invariants output by the source language frontend.
        \item We can use refinement-guided optimizations to transform programs while preserving a specification, such as an error bound. For example, refinement-guided optimization could soundly transform numerical programs in ways that change their results while respecting error bounds set by the frontend. In contrast, current systems, such as \tty{-ffast-math}, can be arbitrarily wrong.
    \end{itemize}
    \TODO{textify the above, revise}
}

\qa{
    Why do we want refinement types for an IR (e.g. as opposed to refinement types for a source language and a compilation-correctness or translation validation setup from that)?   
}{
    \begin{itemize}
        \item Refinement types on an IR allow refinement-guided optimization; this is especially relevant for heavily numeric code, which could gain significantly from a more rigorous \tty{-ffast-math}.
        \item We can use refinement types on an IR as a primitive for translation validation. For example, we can prove that our high-level SQL queries compute correctly by writing that as a refinement and then proving the refinement. Then we can optimize our queries in the low-level IR using these same refinements.
        \item We can use our system of refinement types to prove primitives like data structures, allocators, and runtime functions correctly while clearly stating their specifications. We can then define sound user-specified optimizations with the abstract operator on these primitives.
        \item For a "sufficiently low-level language," we could probably map a system of refinement types in the language somewhat directly to our refinement type system, but this is, for now, strictly speculative.
    \end{itemize}
    \TODO{textify the above, revise}
}

\qa{
    How are IR refinement types going to be produced (presumably not written manually)?
}{
    \begin{itemize}
        \item We plan for most refinements to be emitted by the frontend of programming languages targeting our IR, such as for numeric code subject to fast math optimizations.
        \item Some refinement types for runtime and data structure primitives may be written and proven manually, for example, for functions called by the code emitted by frontends.
        \item Other refinement types may be inserted automatically by analysis passes, which later optimization passes may use. For example, a function may be proven pure.
    \end{itemize}
    \TODO{textify the above, revise}
}

\qa{
    If this is an IR for systems programs, why are simple implementations of 
    relational algebra or linear algebra a good demonstration of viability?
}{
    \begin{itemize}
        \item We mean for the IR to be a systems programming language for reasons of universality rather than specialization; hence, supporting both relational algebra and linear algebra shows that it generalizes at least to a degree.
        \item Demonstrating refinements and refinement-guided optimizations on systems programs is difficult since it requires designing a refinement system over the systems programming language unless we choose a low-level Rust-like language and refine over borrow-checking information. We might do this instead of the relational algebra idea; this would also more readily generalize to linear algebra.
    \end{itemize}
    \TODO{textify the above, revise}
}

\end{itemize}

\addcontentsline{toc}{section}{References}
\printbibliography

\end{document}