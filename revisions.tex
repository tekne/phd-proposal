\documentclass[11pt]{article}

% Layout
\usepackage{setspace}
\usepackage[margin=3cm]{geometry}

% Imports
\usepackage{isotope}

% ERT-specific notation
\newcommand{\stlc}{\(\lambda_{\ms{stlc}}\)}
\newcommand{\ert}{{\(\lambda_{\ms{ert}}\)}}

% Bibliography resources
\addbibresource{references.bib}

\title{PhD Proposal Revisions}
\author{Jad Elkhaleq Ghalayini}

\begin{document}

\maketitle

\tableofcontents

\newpage

\section{Thesis Overview}

The main objective of my thesis is to explore the usage of refinement typing and categorical semantics in the context of a low-level intermediate representation for systems programming. An ideal result would be an intermediate representation equipped with axiomatic semantics serving as a suitable abstraction for a wide range of underlying platforms and memory models. I plan to split the work of my thesis up into two phases: ``Phase 1," in which I develop the core tools, \tty{isotope} and \tty{nucleus}, and ``Phase 2," in which I explore one of two potential research directions, namely, concurrency (``Plan A") and refinement-guided optimization and verification (``Plan B"). The main planned contributions of Phase 1 would be to design and implement \tty{isotope}, an RVSDG-based \cite{rvsdg} low-level intermediate representation equipped with categorical semantics based on the formalism of partially additive symmetric monoidal categories \cites{cat-imp}{flow-cats}, and, using the approach in \cite{ftrs}, enrich it with a refinement type system, \tty{nucleus}, based on explicit proofs (as in our work on Explicit Refinement Types). In Phase 2, we plan to explore one of the possible research directions, depending on which we find to be most promising, and develop \tty{isotope}/\tty{nucleus} into a more robust and usable system to demonstrate the viability of our approach. We plan to complete our implementation work in Rust and our formalization work using a combination of pen and paper proofs and Lean 4. In summary, our intended contributions are:
\begin{itemize}
    \item Core contributions: 
    \begin{itemize}
        \item Develop \tty{isotope}, an RVSDG-based intermediate representation for systems programs
        \item Equip \tty{isotope} with axiomatic categorical semantics, and show this is sound by providing applicable models
        \item Demonstrate that these semantics allow us to perform various standard program optimizations
        \item Equip \tty{isotope} with a system of explicit refinement types, \tty{nucleus}
        \item Show how \tty{nucleus} can be used, via an \tty{abstract} operator, to enable refinement-guided optimizations.
        \item To demonstrate the viability of our our toolchain, implement a simple, strongly-typed functional language which compiles to \tty{isotope}/\tty{nucleus} and supports translation validation and refinement-guided high-level functional optimizations.
    \end{itemize}
    \item Plan A:
    \begin{itemize}
        \item Extend \tty{isotope}/\tty{nucleus} to support a notion of concurrency and appropriate operations (e.g., atomic reads/writes, fences)
        \item Explore the possibility of reasoning about weak memory using categorical semantics for event structures
    \end{itemize}
    \item Plan B:
    \begin{itemize}
        \item Explore how refinement information can be effectively utilized and verified at the IR level by exploring \textit{refinement-aware optimizations} and \textit{tactics/automation techniques}, respectively.
        \item As a case study which is interesting in it's own right, focus on applications to numerical analysis and \tty{-ffast-math} style optimizations
    \end{itemize}
\end{itemize}

% \begin{itemize}
%     \item Goal of thesis: construct an intermediate representation, \tty{isotope}, with a clear categorical semantics, and use this to construct a functor-based refinement type system \tty{nucleus} following the method in \cite{ftrs} supporting \textit{explicit proofs} based on our previous work on explicit refinement types.
%     \item Potential subgoals of thesis:
%     \begin{itemize}
%         \item Provide an IR better suited to giving concurrent programs a clear semantics that can be reasoned about (in a machine-checked way via the refinement system)
%         \item Explore how refinement information can be effectively used and verified at the IR level by exploring \textit{optimizations} and \textit{tactics/automation techniques} respectively.
%     \end{itemize}
%     \item Toolchain:
%     \begin{itemize}
%         \item Formalization in Lean 4
%         \item Implementation in Rust
%     \end{itemize}
% \end{itemize}

\section{Publication Roadmap}

I plan to split up the contributions in my thesis into four to five published papers, with three publications laying out the ``core contributions" and two publications as part of either ``Plan A" or ``Plan B."  We provide a dependency graph of the planned publications in Figure \ref{fig:depg}. The paper highlighted in green, ``Explicit Refinement Types," has already been completed and is currently under review; we provide an overview of its contributions in Section \ref{ssec:ert}. The following sections briefly describe the planned contributions of a particular publication in the dependency graph; note that we plan to write only a \textit{subset} of these papers over the course of the thesis, as indicated by the shading in the dependency graph.

\begin{figure}[H]
    \centering
    \tikzfig{publication-roadmap}
    \caption{Publication roadmap for the \tty{isotope} project}
    \label{fig:depg}
\end{figure}

\subsection{Explicit Refinement Types}

\label{ssec:ert}

In this paper, we provide a simple example of enriching a programming language, here an effectful variant of the simply-typed lambda calculus, with a system of explicit refinement types, pairing refinements with manual mechanized proofs of their correctness. We may view our approach to refining a type system as essentially an ``inlining" of the abstract categorical machinery presented in \cite{ftrs} for a straightforward case. While the presence of explicit proofs means that functions and propositions look dependently typed, this system is not a traditional dependent type theory, as, in particular, there is no definitional equality. Since our proofs are entirely manual, our refinement logic can be extremely rich, and, in particular, we support full first-order logic with quantifiers. Approaching dependent typing from the angle of refinement typing also gives us the advantage that we may easily give our type system denotational semantics by using the semantics of the unrefined calculus, with refinements themselves reducing to predicates on this semantics. The project overall is intended to demonstrate the broad approach planned for the \tty{isotope} project, and in particular the idea of explicit refinements, which is the strategy we plan to use in \tty{nucleus} (with an LCF-style kernel).

\paragraph{Contributions}
\begin{itemize}
        \item We take a simply-typed effectful lambda calculus, \stlc{}, and add a refinement type discipline to it, to obtain the \ert{} language.
        \item We support a rich logic of properties, including full first-order quantifiers, as well as ghost variables/arguments. It does not rely on automated proof, and instead features \emph{explicit} proofs of all properties.
        \item We show that this type system satisfies the expected properties of a type system, such as the syntactic substitution property.   
        \item We give a denotational semantics for both the simply-typed and refined calculi, and we prove the soundness of the intepretations. Using this, we establish semantic regularity of typing, which shows that every program respects the refinement discipline.
        \item We mechanise our semantics and proofs in Lean 4.
\end{itemize}

\subsection{\tty{isotope}: Categorical Semantics for the RVSDG}

The RVSDG, or Regionalized Value-State Dependence Graph, is an intermediate representation designed to make standard optimizations more efficient by eliminating the overhead of repeated restoration of SSA form after optimization passes \cite{rvsdg}. The RVSDG also has the advantage of directly exposing dataflow dependencies between instructions, which are otherwise unordered, simplifying the implementation of optimization passes requiring this information and making the IR highly normalizing. We aim to show that these advantages come from the RVSDG having more compositional semantics than SSA form. To do so, we plan to give a categorical axiomatization of the sequential semantics of (a slight generalization of) RVSDGs in terms of partially additive categories, as in \cites{flow-cats}{cat-imp}. We then aim to validate these semantics by showing that the usual relational semantics of stateful programs form a model of this category and that a collection of standard optimizations, such as mem2reg, value numbering, and sparse conditional constant propagation, have axiomatic correctness proofs. Finally, we wish to show that there is no asymptotic cost for using RVSDGs due to the existence of fast algorithms converting programs in SSA form to and from RVSDGs.

\paragraph{Planned Contributions}
\begin{itemize}
    \item A Rust implementation of the \tty{isotope} intermediate representation, based on a slightly generalized RVSDG, supporting:
    \begin{itemize}
        \item Standard optimizations such as dead code elimination, \tty{mem2reg}, scalar replacement of aggregates, sparse conditional constant propagation, and value numbering
        \item Conversion to and from SSA form
        \item An LLVM compilation backend
        \item A test suite
    \end{itemize}
    \item Basic benchmarks of \tty{isotope}'s performance and compilation times, w.r.t LLVM and Cranelift
    \item A categorical semantics for \tty{isotope} based on \cite{flow-cats}, formalized in Lean 4, along with mechanized proofs of the soundness of the some optimizations as well as the conversion to SSA
    \item Pen-and-paper proofs of the soundness of a few more complex optimizations
    \item A simple, relational model for the categorical semantics of \tty{isotope} representing sequential execution of \tty{isotope} programs
\end{itemize}

\subsection{\tty{nucleus}: Refinement Types for \tty{isotope}}

This paper aims to enrich the \tty{isotope} language with a system of refinement types, \tty{nucleus}, built over the axiomatic categorical semantics following the approach in \cite{ftrs}. In particular, following our work on ERT, we plan to use a system of explicit refinement types with an LCF-style proof construction API, which we may view as an ``intermediate representation" for a higher-level tactic or solver-based API. We then plan to extend \tty{isotope} with an \tty{abstract} operator, which, given a refinement and a value satisfying it, semantically corresponds to any value satisfying that refinement. This operator is trivial to compile since we can reduce it to the provided value as a valid lowering but, semantically, drastically widens the scope of allowable optimizations. We then aim to show the practical utility of such a system (given that refinement types are usually over surface languages rather than intermediate representations) by demonstrating the feasibility of refinement-guided optimizations and simple translation validation. In particular, we wish to develop a simple in-memory implementation of relational algebra compiling to \tty{isotope} while automatically generating \tty{nucleus} proofs that its programs, given well-formed data structures representing relations as input, yield well-formed outputs without undefined behavior. To show the viability of refinement-guided optimizations, we plan to implement high-level relational optimizations on \tty{abstract}'ed implementations of the aforementioned specifications, integrating this functionality into the toy compiler.

\paragraph{Planned Contributions}

\begin{itemize}
    \item A program logic similar to separation logic for sequential \tty{isotope}, \tty{nucleus}
    \item A semantically well-defined \tty{abstract} operator for \tty{isotope} based on \tty{nucleus} specifications
    \item A system of \textit{explicit} refinement types for \tty{isotope}, with semantics based on \tty{isotope}'s categorical semantics based on the methods in \cite{ftrs} (in particular, w.r.t. Hoare logic and monoidal categories)
    \item A Rust implementation of a proof checker for refined \tty{isotope} programs with an LCF-like API
    \item A proof of the soundness of \tty{nucleus} in Lean 4
    \item A simple implementation of relational algebra compiling to verified, refined \tty{isotope}
    \item A set of high-level optimizations for \tty{abstract}'ed specifications, implemented in Rust and proved sound in Lean 4
\end{itemize}

\subsection{Concurrent \tty{isotope}}

This paper aims to generalize the \tty{isotope} language and categorical semantics to support a notion of concurrency by introducing threads and atomic operations, along with associated axiomatic semantics. While we plan to start by considering only sequentially consistent execution, we aim to generalize this significantly and consider relaxed atomics. However, getting the semantics to work for these in a compositional way may prove to be an insurmountable obstacle. To tackle this challenge, we hope to draw on work relating to the category of event structures (e.g., \cite{event-structures}), denotational semantics for weak memory (e.g., \cite{weak-memory-densem}), and semantics for sequential composition in the weak memory setting (e.g., \cite{leaky-semicolon}). This paper's primary contribution would be to give a concrete model for each concurrent setting and show that it obeys our desired axiomatic semantics. By keeping the axiomatic semantics as close as possible to the original isotope semantics, we hope to preserve the validity of at least the majority of the optimizations proven sound in the sequential setting when applied to unsynchronized memory operations. We can also analyze how well these optimizations generalize, e.g., to sequentially consistent operations. Another critical topic to examine would be whether we can define a sound and efficient compilation pipeline to platforms such as LLVM based on the standard C11 semantics while avoiding the need to insert excessive synchronization or fencing.

\paragraph{Planned Contributions}

\begin{itemize}
    \item A set of operations extending \tty{isotope} to support threading and atomics, with associated axiomatic semantics
    \item A concrete categorical model for these semantics representing sequentially consistent concurrent execution
    \item A concrete categorical model for these semantics representing relaxed-memory concurrency
    \item An evaluation of how the optimizations from the sequential \tty{isotope} paper hold up in the new axiomatic setting
    \item A sound compilation from concurrent \tty{isotope} to LLVM
\end{itemize}

\subsection{Concurrent \tty{nucleus}}

This paper aims to follow up on the previous paper by extending \tty{nucleus} to support reasoning about concurrent \tty{isotope} programs. Doing so will require adjusting the refinement type system to work with the new concurrent operations and semantics and exploring how we can effectively modify the logic to reason about concurrent programs. We plan to take inspiration from the extensive work in the Iris \cite{iris} world on program logics for concurrent C, particularly the RefinedC \cite{refinedc} project, which integrates ownership types with refinement types while producing foundational proofs. We hope to be able to reason about at least simple concurrent data structures such as a spinlock or atomic linked list. Another design goal in extending \tty{nucleus} to support concurrency is to ensure we still support at least most sequential \tty{nucleus} proofs. In particular, we hope to port the relational agebra compiler from the sequential \tty{nucleus} paper to the new concurrent setting, while potentially adding new features to support concurrent execution (which should hopefully be tractable in a simple setting).

\paragraph{Planned Contributions}

\begin{itemize}
    \item An adaptation and extension of the \tty{nucleus} program logic and refinement type system to deal with concurrency
    \item A Rust implementation of the extended system
    \item A proof of the soundness of the resulting system w.r.t. the new semantics of \tty{isotope}
    \item A set of simple verified concurrent data structures, such as spinlocks, atomic linked lists, and channels
    \item A port of the query language from the sequential \tty{nucleus} paper to concurrent \tty{nucleus}, with potential support for concurrent execution of queries. 
\end{itemize}

\subsection{Refinement-guided Optimization for \tty{nucleus}}

\label{ssec:ref-guided-opt}

In this paper, we plan to extend our work on refinement-guided optimizations for isotope/nucleus by building a framework for constructing sound, user-defined refinement-guided optimizations of \tty{abstract}'ed programs. We plan to do this by relying on the relatively straightforward principle that the program \tty{abstract}\((P, \varphi)\) can be optimized to \tty{abstract}\((Q, \psi)\) if we can prove \(\psi \implies \varphi\). As a practical use case, we plan to implement \tty{-ffast-math} like optimizations on floating points given as \tty{abstract}'ed specifications of their acceptable range of values; this allows us to reason about and verify error bounds while still allowing complex non-deterministic optimization strategies. We plan to scale up this approach to reasoning about linear algebra primitives, such as those in BLAS and MKL. The end goal is to allow performing high-level optimizations of numerically demanding programs such as neural networks and simulation software while maintaining mathematically rigorous error bounds. To demonstrate this, we aim to implement a first-order language with floating-point and linear-algebra support, extended with the ability to specify allowable error bounds for a wrapped operation.

\paragraph{Planned Contributions}

\begin{itemize}
    \item A system for user-defined refinement-guided optimizations for \tty{isotope}/\tty{nucleus} programs, implemented in Rust
    \item A proof of the soundness of this system in Lean 4
    \item An implementation of \tty{-ffast-math} like floating-point optimizations using this system
    \item An implementation of high-level linear-algebra optimizations on BLAS/MKL primitives using this system
    \item An implementation of a toy language  supporting linear algebra and ``error bound blocks" around numerical expressions
\end{itemize}

\subsection{Tactics for \tty{nucleus}}

In this paper, we focus on taking our previous work on \tty{nucleus} and attempting to convert it into a more usable system for verification work by implementing a set of tactics for automatically discharging verification conditions. In particular, we plan to implement variants of proof search, congruence closure, and other common tactics and integrate automated theorem provers such as SMT solvers. For simplicity, we will use solvers as trusted components by accepting their outputs as axioms (rather than using the outputs to guide proof search, as in Isabelle's sledgehammer tool). To demonstrate the utility of such a system, we aim to extend our linear algebra language with a simple system of refinement types, which we plan to lower to corresponding \tty{nucleus}-based refinement types for verification.

In particular, we aim to be able to automatically prove that simple \tty{unsafe} programs in our functional language are UB-free by checking, e.g., array indexing, by lowering to refined \tty{isotope} and then attempting to have our automation discharge the required proof obligations. If time permits, we hope to explore a system to allow, in some cases, ``bubbling-up'' unprovable verification conditions to the user without exposing low-level \tty{isotope} details as a proof-of-concept, which would allow both cleaner error messages and the potential for manual proofs. This should be possible since most of the verification conditions generated by our simple language should relate to pure arithmetic. Another possible extension is to further explore the \tty{-ffast-math} use case described in Section \ref{ssec:ref-guided-opt} by automatically discharging simple reasoning about the error bounds of arithmetic functions.

\paragraph{Planned Contributions}

\begin{itemize}
    \item Implementations of various tactics using \tty{nucleus}'s Rust API
    \item Implementation of SMT-solver/\tty{nucleus} integration using the Rust API
    \item An extension of the linear algebra language to support a simple system of refinement types, with compilation to \tty{isotope} and automatic discharge of verification conditions
    \item Possible extension: ``bubbling up'' of simple arithmetic verification conditions the solver was unable to discharge for use in error messages and/or manual proof by the user, as a proof-of-concept
    \item Possible extension: reasoning automatically about the error bounds of numerical code
\end{itemize}

\addcontentsline{toc}{section}{References}
\printbibliography

\end{document}