\documentclass[11pt]{article}

% Layout
\usepackage{setspace}
\usepackage[margin=3cm]{geometry}

% Imports
\usepackage{isotope}

% ERT-specific notation
\newcommand{\stlc}{\(\lambda_{\ms{stlc}}\)}
\newcommand{\ert}{{\(\lambda_{\ms{ert}}\)}}

% Bibliography resources
\addbibresource{references.bib}

\title{PhD Proposal Revisions}
\author{Jad Elkhaleq Ghalayini}

\begin{document}

\maketitle

The purpose of a compiler intermediate representation is to provide a convenient way to manipulate programs for rewriting; for the compiler to be sound, we must have some means of knowing whether these rewrites are semantics-preserving. The most common IR for modern compilers is SSA, which has well-understood properties and semantics. The properties of SSA are well-suited to perform analysis passes and equational rewrites on pure code (such as is exploited by classic optimization passes like sparse conditional constant propagation). Unfortunately, SSA does not make the state dependencies between instructions explicit. Instead, it implicitly encodes this information in the instruction order, making it difficult to reason about programs in SSA form compositionally. Hence, even simple rewrites become prohibitively difficult to reason about in the presence of complex side effects such as access to shared memory. 

The RVSDG is a new intermediate representation introduced in \cite{rvsdg} to make optimizations more efficient by making all data and state dependencies explicit as edges in a graph of instructions. We have noticed that, due to this property, RVSDGs have "good" semantics. In particular, RVSDG nodes, due to only depending on their inputs and having all side-effects tracked by their outputs, have naturally compositional semantics, which makes validating equations easier. Furthermore, these semantics have a natural categorical/axiomatic phrasing, making it easier to give multiple concrete denotational models. We want multiple models to provide semantics for sequential, sequentially consistent, and (hopefully) weak memory without redoing all our work. Another advantage of the RVSDG is that, while novel, they support efficient conversion of programs to and from SSA form, allowing us to take advantage of existing, mature SSA-based toolchains such as LLVM rather than needing to re-invent the wheel.

While this is sufficient to reason about unconditional equations, many optimizations we want to do are context/value-dependent. For example, when multiplying the elements in a list, if one element is known to be zero, the entire loop can be optimized to zero. Refinement types enrich a type system to track this kind of information: for example, we can create the type of all lists with at least one zero element and then check whether or not a list lies in this type. Refinement types also play well with categorical semantics, for example, via the techniques in \cite{ftrs}. We can use refinement types over an IR both to represent and validate the information that analysis passes discover, giving us a semantically rigorous way to tag values with various kinds of information and assertions. In particular, this makes it possible to replace the ad-hoc sets of pre-defined tags (such as "noalias") used by modern compilers with refinements. 

One exciting avenue this opens up is the ability to decompose combined analysis/transformation passes into simpler chunks. For example, we may view sparse conditional constant propagation as refining values \(v\) with one of the predicates \(\top\), \(v = e\), or \(\bot\) (unreachable) and then performing refinement-guided dead-code elimination and constant propagation as later phases. Refinement types can also let us push high-level information down into a low-level IR to allow performing optimizations that are sound w.r.t high-level but not low-level semantics on low-level IR source. That is, we can replace a program term with a different term which meets the high-level specification, but which does not have equivalent low-level behaviour. 
For example, we may specify the output of mathematical operations such as matrix multiplication to be within \(\epsilon\) of the abstract mathematical value it would otherwise take. Doing so could allow us to safely perform algebraic rewrites unsound on IEEE floating points, such as re-association or replacing addition and multiplication with an FMA, improving run-time and numerical stability while maintaining rigorous error bounds. In particular, allowing low-level optimizations and high-level linear-algebra-based rewrites to occur within the same IR may have various applications, e.g., optimizing subroutines for machine learning.

\printbibliography

\end{document}